package com.epam.lab.ship.with.droids.array;

import java.util.Arrays;

public class Ship<T extends Droid> {
    private T[] droids;

    public Ship() {
        droids = (T[]) new Droid[1];
    }

   public void put(T droid){
        if(droids[droids.length - 1] != null) {
            droids = Arrays.copyOf(droids, droids.length + 1);
            droids[droids.length - 1] = droid;
        } else droids[droids.length - 1] = droid;
    }
    public void get(T droid){
        boolean isExist = false;
        for (int i = 0; i < droids.length; i++) {
            if (droids[i] == droid){
                isExist = true;
                if(i < droids.length - 1){
                    droids[i] = droids[i + 1];
                }
            }
        }
        if(isExist){
            droids = Arrays.copyOf(droids, droids.length - 1);
        }
    }

    public void show(){
        for (Droid droid: droids) {
            System.out.println(droid);
        }
    }
}
