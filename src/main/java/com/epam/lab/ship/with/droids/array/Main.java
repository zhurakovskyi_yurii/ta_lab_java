package com.epam.lab.ship.with.droids.array;


public class Main {
    public static void main(String[] args) {
        Ship<? super Droid> ship = new Ship<>();
        ship.put(new IG_88());
        ship.put(new R2_D2());
        ship.put(new TC());
        ship.show();
    }
}
