package com.epam.lab.hypermarket.app;

/**
 * @author Yurii Zhurakovskyi
 */
public enum Color {
	BLACK, WHITE, BROWN, GREY, DARK_GREEN, DARK_GREY
}
