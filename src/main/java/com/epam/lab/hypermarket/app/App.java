package com.epam.lab.hypermarket.app;

import com.epam.lab.hypermarket.building_materials.Cement;
import com.epam.lab.hypermarket.building_materials.Concrete;
import com.epam.lab.hypermarket.building_materials.Sand;
import com.epam.lab.hypermarket.householdappliancesandconsumerelectronics.FoodProcessor;
import com.epam.lab.hypermarket.householdappliancesandconsumerelectronics.Fridge;
import com.epam.lab.hypermarket.householdappliancesandconsumerelectronics.SmartTV;
import com.epam.lab.hypermarket.plumbing.Bathtub;
import com.epam.lab.hypermarket.plumbing.Toilet;
import com.epam.lab.hypermarket.plumbing.Type_Of_Toilet;
import com.epam.lab.hypermarket.plumbing.Washbasin;
import com.epam.lab.hypermarket.wood_products.Door;
import com.epam.lab.hypermarket.wood_products.Floor;
import com.epam.lab.hypermarket.wood_products.Wood;

/**
 * @author Yurii Zhurakovskyi
 *
 */
public class App {

	public static void main(String[] args) {

		MyHypermarket myHypermarket = MyHypermarket.getInstance();
		fillTheHypermarketWithGoods(myHypermarket);
		System.out.println(myHypermarket + "\n\n\n");

		System.out.println("Find by type and price less than or equal 1000: \n");
		myHypermarket.findByTypeAndPriceLessThanOrEqual(Type.CONSUMER_ELECTRONICS_AND_HOUSEHOLD_APPLIANCES.name(), 1000)
				.stream().forEach(e -> System.out.println(e + "\n"));

		System.out.println("\nFind by type and price more than or equal 4000: \n");
		myHypermarket.findByTypeAndPriceMoreThanOrEqual(Type.WOOD_PRODUCTS.name(), 4000).stream()
				.forEach(e -> System.out.println("\n " + e));
	}

	public static HypermarketEntity fillTheHypermarketWithGoods(HypermarketEntity entity) {
		entity.addGood(new FoodProcessor("name", Country.AUSTRALIA, new Brand("BN9", "addr9"), 1000, 3,
				Material.PLASTIC, 20, 40, 20, 100));
		entity.addGood(new FoodProcessor("name", Country.AUSTRALIA, new Brand("BN9", "addr9"), 2000, 3,
				Material.PLASTIC, 20, 40, 20, 100));
		entity.addGood(new FoodProcessor("name", Country.AUSTRALIA, new Brand("BN9", "addr9"), 5000, 3,
				Material.PLASTIC, 20, 40, 20, 100));

		entity.addGood(new Cement("name", Country.BELGIUM, 1100, new Brand("BN9", "addr9"), 300));
		entity.addGood(new Cement("name", Country.BELGIUM, 1100, new Brand("BN9", "addr9"), 300));
		entity.addGood(new Cement("name", Country.BELGIUM, 1100, new Brand("BN9", "addr9"), 300));
		entity.addGood(new Cement("name", Country.BELGIUM, 1100, new Brand("BN9", "addr9"), 300));
		entity.addGood(new Cement("name", Country.BELGIUM, 1100, new Brand("BN9", "addr9"), 300));

		entity.addGood(new Concrete("name", Country.BELGIUM, 1100, new Brand("BN9", "addr9"), 300));
		entity.addGood(new Concrete("name", Country.BELGIUM, 1100, new Brand("BN9", "addr9"), 300));
		entity.addGood(new Concrete("name", Country.BELGIUM, 1100, new Brand("BN9", "addr9"), 300));
		entity.addGood(new Concrete("name", Country.BELGIUM, 1100, new Brand("BN9", "addr9"), 300));
		entity.addGood(new Concrete("name", Country.BELGIUM, 1100, new Brand("BN9", "addr9"), 300));

		entity.addGood(new Door("name1", new Brand("BN10", "addr19"), 200, 1000, 10, Color.BROWN, Country.GERMANY, 3000,
				Wood.BEECH));
		entity.addGood(new Door("name2", new Brand("BN10", "addr19"), 200, 1000, 10, Color.BROWN, Country.GERMANY, 4000,
				Wood.BEECH));
		entity.addGood(new Door("name3", new Brand("BN10", "addr19"), 200, 1000, 10, Color.BROWN, Country.GERMANY, 6000,
				Wood.BEECH));
		entity.addGood(new Door("name4", new Brand("BN10", "addr19"), 200, 1000, 10, Color.BROWN, Country.GERMANY, 1000,
				Wood.BEECH));
		entity.addGood(new Door("name5", new Brand("BN9", "addr9"), 200, 1000, 10, Color.BROWN, Country.GERMANY, 1000,
				Wood.BEECH));
		entity.addGood(new Door("name6", new Brand("BN10", "addr19"), 200, 1000, 10, Color.BROWN, Country.GERMANY, 2000,
				Wood.BEECH));
		entity.addGood(new Door("name7", new Brand("BN10", "addr19"), 200, 1000, 10, Color.BROWN, Country.GERMANY, 4000,
				Wood.BEECH));
		entity.addGood(new Door("name8", new Brand("BN10", "addr19"), 200, 1000, 10, Color.BROWN, Country.GERMANY, 3000,
				Wood.BEECH));
		entity.addGood(new Door("name9", new Brand("BN10", "addr19"), 200, 1000, 10, Color.BROWN, Country.GERMANY, 3000,
				Wood.BEECH));
		entity.addGood(new Door("name10", new Brand("BN9", "addr9"), 200, 1000, 10, Color.BROWN, Country.GERMANY, 1000,
				Wood.BEECH));
		entity.addGood(new Door("name11", new Brand("BN9", "addr9"), 200, 1000, 10, Color.BROWN, Country.GERMANY, 1000,
				Wood.BEECH));
		entity.addGood(new Door("name12", new Brand("BN9", "addr9"), 200, 1000, 10, Color.BROWN, Country.GERMANY, 1000,
				Wood.BEECH));
		entity.addGood(new Door("name13", new Brand("BN9", "addr9"), 200, 1000, 10, Color.BROWN, Country.GERMANY, 2000,
				Wood.BEECH));
		entity.addGood(new Door("name14", new Brand("BN9", "addr9"), 200, 1000, 10, Color.BROWN, Country.GERMANY, 3000,
				Wood.BEECH));
		entity.addGood(new Door("name15", new Brand("BN9", "addr9"), 200, 1000, 10, Color.BROWN, Country.GERMANY, 2000,
				Wood.BEECH));

		entity.addGood(new Floor("name1", Country.AUSTRIA, new Brand("floor1", "address111"), 1000, Wood.OAK, 1000, 200,
				10, 5));
		entity.addGood(new Floor("name2", Country.AUSTRIA, new Brand("floor2", "address111"), 1000, Wood.ELM, 1000, 200,
				10, 5));
		entity.addGood(new Floor("name3", Country.BELGIUM, new Brand("floor3", "address111"), 1000, Wood.MAHOGANY, 1000,
				200, 10, 5));
		entity.addGood(new Floor("name4", Country.AUSTRIA, new Brand("floor4", "address111"), 1000, Wood.PINE, 1000,
				200, 10, 5));
		entity.addGood(new Floor("name5", Country.AUSTRIA, new Brand("floor5", "address111"), 1000, Wood.TEAK, 1000,
				200, 10, 5));
		entity.addGood(new Floor("name6", Country.AUSTRIA, new Brand("floor6", "address111"), 1000, Wood.FIR, 1000, 200,
				10, 5));
		entity.addGood(new Floor("name7", Country.AUSTRIA, new Brand("floor7", "address111"), 1000, Wood.WALNUT, 1000,
				200, 10, 5));
		entity.addGood(new Floor("name8", Country.AUSTRIA, new Brand("floor8", "address111"), 1000, Wood.WALNUT, 1000,
				200, 10, 5));
		entity.addGood(new Floor("name9", Country.AUSTRIA, new Brand("floor9", "address111"), 1000, Wood.ELM, 1000, 200,
				10, 5));
		entity.addGood(new Floor("name10", Country.AUSTRIA, new Brand("floor10", "address111"), 1000, Wood.OAK, 1000,
				200, 10, 5));

		entity.addGood(new Washbasin("1WB", new Brand("BrandName1", "address1"), Country.AUSTRALIA, 1000, Color.BLACK,
				Material.CERAMIC, 500, 400, 200));
		entity.addGood(new Washbasin("2WB", new Brand("BrandName2", "address2"), Country.AUSTRIA, 2000, Color.BLACK,
				Material.CERAMIC, 480, 800, 200));
		entity.addGood(new Washbasin("3WB", new Brand("BrandName3", "address3"), Country.BELGIUM, 3000, Color.BLACK,
				Material.CERAMIC, 300, 600, 300));
		entity.addGood(new Washbasin("4WB", new Brand("BrandName4", "address4"), Country.CANADA, 4000, Color.BLACK,
				Material.CERAMIC, 500, 600, 400));
		entity.addGood(new Washbasin("5WB", new Brand("BrandNamet5", "address5"), Country.FINLAND, 5000, Color.BLACK,
				Material.CERAMIC, 300, 200, 140));

		entity.addGood(new Sand("name1", Country.BELGIUM, 1100, new Brand("BN9", "addr9"), 300));
		entity.addGood(new Sand("name2", Country.BELGIUM, 1100, new Brand("BN9", "addr9"), 300));
		entity.addGood(new Sand("name3", Country.BELGIUM, 1100, new Brand("BN9", "addr9"), 300));
		entity.addGood(new Sand("name4", Country.BELGIUM, 1100, new Brand("BN9", "addr9"), 300));
		entity.addGood(new Sand("name5", Country.BELGIUM, 1100, new Brand("BN9", "addr9"), 300));

		entity.addGood(new Toilet("1T", Country.AUSTRALIA, new Brand("BN1", "addr1"), 1000, Color.WHITE,
				Material.CERAMIC, Type_Of_Toilet.ELONGATED_BOWL_TOILET, 100, 600, 200));
		entity.addGood(new Toilet("2T", Country.AUSTRIA, new Brand("BN2", "addr2"), 2000, Color.BLACK, Material.CERAMIC,
				Type_Of_Toilet.ELONGATED_BOWL_TOILET, 100, 600, 200));
		entity.addGood(new Toilet("3T", Country.BELGIUM, new Brand("BN3", "addr3"), 3000, Color.WHITE, Material.CERAMIC,
				Type_Of_Toilet.ELONGATED_BOWL_TOILET, 100, 600, 200));
		entity.addGood(new Toilet("4T", Country.CANADA, new Brand("BN4", "addr4"), 4000, Color.BLACK, Material.CERAMIC,
				Type_Of_Toilet.ELONGATED_BOWL_TOILET, 100, 600, 200));
		entity.addGood(new Toilet("5T", Country.FINLAND, new Brand("BN5", "addr5"), 5000, Color.WHITE, Material.CERAMIC,
				Type_Of_Toilet.ELONGATED_BOWL_TOILET, 100, 600, 200));

		entity.addGood(new Bathtub("1B", Country.AUSTRALIA, new Brand("BN1", "addr1"), 1000, Color.WHITE,
				Material.CERAMIC, 100, 600, 200));
		entity.addGood(new Bathtub("2B", Country.AUSTRALIA, new Brand("BN2", "addr1"), 2000, Color.WHITE,
				Material.CERAMIC, 100, 600, 200));
		entity.addGood(new Bathtub("3B", Country.AUSTRALIA, new Brand("BN3", "addr1"), 3000, Color.WHITE,
				Material.CERAMIC, 100, 600, 200));
		entity.addGood(new Bathtub("4B", Country.AUSTRALIA, new Brand("BN4", "addr1"), 4000, Color.WHITE,
				Material.CERAMIC, 100, 600, 200));
		entity.addGood(new Bathtub("5B", Country.AUSTRALIA, new Brand("BN5", "addr1"), 5000, Color.WHITE,
				Material.CERAMIC, 100, 600, 200));

		entity.addGood(new Fridge("1F", Country.AUSTRALIA, new Brand("BN1", "addr1"), 1000, 500, 120, Color.BLACK,
				"rectangular ", 400, 100, 600, 200));
		entity.addGood(new Fridge("2F", Country.AUSTRALIA, new Brand("BN2", "addr2"), 2000, 500, 120, Color.BLACK,
				"rectangular ", 400, 100, 600, 200));
		entity.addGood(new Fridge("3F", Country.AUSTRALIA, new Brand("BN3", "addr3"), 3000, 500, 120, Color.BLACK,
				"rectangular ", 400, 100, 600, 200));
		entity.addGood(new Fridge("4F", Country.AUSTRALIA, new Brand("BN4", "addr4"), 4000, 500, 120, Color.BLACK,
				"rectangular ", 400, 100, 600, 200));
		entity.addGood(new Fridge("5F", Country.AUSTRALIA, new Brand("BN5", "addr5"), 5000, 500, 120, Color.BLACK,
				"rectangular ", 400, 100, 600, 200));

		entity.addGood(new SmartTV("smartTV1", Country.AUSTRALIA, new Brand("BN8", "addr8"), 10000, "screen_size",
				"display_format", "wide_screen", "backlight", "display"));
		entity.addGood(new SmartTV("smartTV2", Country.AUSTRALIA, new Brand("BN8", "addr8"), 12000, "screen_size",
				"display_format", "wide_screen", "backlight", "display"));
		entity.addGood(new SmartTV("smartTV3", Country.AUSTRALIA, new Brand("BN8", "addr8"), 13000, "screen_size",
				"display_format", "wide_screen", "backlight", "display"));
		entity.addGood(new SmartTV("smartTV4", Country.AUSTRALIA, new Brand("BN8", "addr8"), 15000, "screen_size",
				"display_format", "wide_screen", "backlight", "display"));

		return entity;
	}
}
