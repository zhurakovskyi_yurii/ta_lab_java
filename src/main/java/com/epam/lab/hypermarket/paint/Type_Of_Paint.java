package com.epam.lab.hypermarket.paint;

public enum Type_Of_Paint {
	MATTE_PAINT, MATTE_ENAMEL, SATIN, EGGSHELL, SEMI_GLOSS, GLOSS_PAINT
}
