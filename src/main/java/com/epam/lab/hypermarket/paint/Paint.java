package com.epam.lab.hypermarket.paint;

import com.epam.lab.hypermarket.app.Brand;
import com.epam.lab.hypermarket.app.Color;
import com.epam.lab.hypermarket.app.Country;
import com.epam.lab.hypermarket.app.Good;
import com.epam.lab.hypermarket.app.Type;

public class Paint extends Good {

	private double amount;
	private Type_Of_Paint type_Of_Paint;
	private Color color;

	public Paint(String name, Country country, Brand brand, double price, double amount, Type_Of_Paint type_Of_Paint,
			Color color) {
		super(Type.PAINTS_AND_VARNISHES, name, country, price, brand);
		this.amount = amount;
		this.type_Of_Paint = type_Of_Paint;
		this.color = color;
	}

	public double getAmount() {
		return amount;
	}

	public Type_Of_Paint getType_Of_Paint() {
		return type_Of_Paint;
	}

	public Color getColor() {
		return color;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		long temp;
		temp = Double.doubleToLongBits(amount);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + ((type_Of_Paint == null) ? 0 : type_Of_Paint.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Paint other = (Paint) obj;
		if (Double.doubleToLongBits(amount) != Double.doubleToLongBits(other.amount))
			return false;
		if (color != other.color)
			return false;
		if (type_Of_Paint != other.type_Of_Paint)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + ", type of the paint" + type_Of_Paint + ", amount: " + amount + ", color: " + color;
	}
}
