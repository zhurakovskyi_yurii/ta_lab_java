package com.epam.lab.hypermarket.plumbing;

import com.epam.lab.hypermarket.app.Brand;
import com.epam.lab.hypermarket.app.Color;
import com.epam.lab.hypermarket.app.Country;
import com.epam.lab.hypermarket.app.Good;
import com.epam.lab.hypermarket.app.Material;
import com.epam.lab.hypermarket.app.Type;

/**
 * @author Yurii Zhurakovskyi
 */
public class Bathtub extends Good {

	private Color color;
	private Material material;
	private int width;
	private int height;
	private int depth;

	public Bathtub(String name, Country country, Brand brand, double price, Color color, Material material, int width,
			int height, int depth) {
		super(Type.PLUMBING, name, country, price, brand);
		this.color = color;
		this.material = material;
		this.width = width;
		this.height = height;
		this.depth = depth;
	}

	public Color getColor() {
		return color;
	}

	public Material getMaterial() {
		return material;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getDepth() {
		return depth;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + depth;
		result = prime * result + height;
		result = prime * result + ((material == null) ? 0 : material.hashCode());
		result = prime * result + width;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bathtub other = (Bathtub) obj;
		if (color != other.color)
			return false;
		if (depth != other.depth)
			return false;
		if (height != other.height)
			return false;
		if (material != other.material)
			return false;
		if (width != other.width)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + ", color: " + color + ", " + height + "X" + width + "X" + depth + ", material: "
				+ material.name();
	}
}
