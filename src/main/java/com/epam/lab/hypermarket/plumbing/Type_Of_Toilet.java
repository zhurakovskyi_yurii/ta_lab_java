package com.epam.lab.hypermarket.plumbing;

public enum Type_Of_Toilet {
	ROUND_BOWL_TOILET,
	SQUARE_BOWL_TOILET,
	ELONGATED_BOWL_TOILET,
	RECTANGULAR_BOWL_TOILET
}
