package com.epam.lab.hypermarket.wood_products;

import com.epam.lab.hypermarket.app.Brand;
import com.epam.lab.hypermarket.app.Color;
import com.epam.lab.hypermarket.app.Country;
import com.epam.lab.hypermarket.app.Good;
import com.epam.lab.hypermarket.app.Type;

/**
 * @author Yurii Zhurakovskyi
 *
 */
public class Door extends Good {

	private int height;
	private int width;
	private int thickness;
	private Color color;
	private Wood type_of_wood;

	public Door(String name, Brand brand, int height, int width, int thickness, Color color, Country country,
			double price, Wood type_of_wood) {
		super(Type.WOOD_PRODUCTS, name, country, price, brand);
		this.height = height;
		this.width = width;
		this.thickness = thickness;
		this.color = color;
		this.type_of_wood = type_of_wood;
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public int getThickness() {
		return thickness;
	}

	public Color getColor() {
		return color;
	}

	public Wood getType_of_wood() {
		return type_of_wood;
	}

	public void setType_of_wood(Wood type_of_wood) {
		this.type_of_wood = type_of_wood;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + height;
		result = prime * result + thickness;
		result = prime * result + ((type_of_wood == null) ? 0 : type_of_wood.hashCode());
		result = prime * result + width;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Door other = (Door) obj;
		if (color != other.color)
			return false;
		if (height != other.height)
			return false;
		if (thickness != other.thickness)
			return false;
		if (type_of_wood != other.type_of_wood)
			return false;
		if (width != other.width)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + ", " + ", " + height + "X" + width + "X" + thickness + ", type of wood: "
				+ type_of_wood.name();
	}
}
