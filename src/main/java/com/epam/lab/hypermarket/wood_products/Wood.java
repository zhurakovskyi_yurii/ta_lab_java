package com.epam.lab.hypermarket.wood_products;

/**
 * @author Yurii Zhurakovskyi
 *
 */
public enum Wood {
	PINE, CEDAR, FIR, TEAK, WALNUT, MAHOGANY, OAK, BEECH, ELM
}
