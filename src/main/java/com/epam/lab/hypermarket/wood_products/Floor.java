package com.epam.lab.hypermarket.wood_products;

import com.epam.lab.hypermarket.app.Brand;
import com.epam.lab.hypermarket.app.Country;
import com.epam.lab.hypermarket.app.Good;
import com.epam.lab.hypermarket.app.Type;

/**
 * @author Yurii Zhurakovskyi
 */
public class Floor extends Good {

	private int height;
	private int width;
	private int thickness;
	private int count;
	private Wood type_of_wood;

	public Floor(String name, Country country, Brand brand, double price, Wood type_of_wood, int height, int width,
			int thickness, int count) {
		super(Type.WOOD_PRODUCTS, name, country, price, brand);
		this.type_of_wood = type_of_wood;
		this.height = height;
		this.width = width;
		this.thickness = thickness;
		this.count = count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public int getThickness() {
		return thickness;
	}

	public int getCount() {
		return count;
	}

	public Wood getType_of_wood() {
		return type_of_wood;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + count;
		result = prime * result + height;
		result = prime * result + thickness;
		result = prime * result + ((type_of_wood == null) ? 0 : type_of_wood.hashCode());
		result = prime * result + width;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Floor other = (Floor) obj;
		if (count != other.count)
			return false;
		if (height != other.height)
			return false;
		if (thickness != other.thickness)
			return false;
		if (type_of_wood != other.type_of_wood)
			return false;
		if (width != other.width)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + ", " + height + "X" + width + "X" + thickness + ", quantity in the package: " + count
				+ ", type of wood: " + type_of_wood.name();
	}
}
