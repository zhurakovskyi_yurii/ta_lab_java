package com.epam.lab.hypermarket.building_materials;

import com.epam.lab.hypermarket.app.Brand;
import com.epam.lab.hypermarket.app.Country;

public class DecorativeStones extends BuildingMaterial {

	public DecorativeStones(String name, Country country, double price, Brand brand, int weight) {
		super(name, country, price, brand, weight);
	}

	@Override
	public String toString() {
		return super.toString();
	}
}
