package com.epam.lab.hypermarket.building_materials;

import com.epam.lab.hypermarket.app.Brand;
import com.epam.lab.hypermarket.app.Country;
import com.epam.lab.hypermarket.app.Good;
import com.epam.lab.hypermarket.app.Type;

public abstract class BuildingMaterial extends Good {

	private int weight;

	public BuildingMaterial(String name, Country country, double price, Brand brand, int weight) {
		super(Type.BUILDING_MATERIAL, name, country, price, brand);
	}

	@Override
	public String toString() {
		return super.toString() + ", weight" + weight;
	}

}
