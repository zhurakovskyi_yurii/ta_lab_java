package com.epam.lab.hypermarket.building_materials;

import com.epam.lab.hypermarket.app.Brand;
import com.epam.lab.hypermarket.app.Country;
import com.epam.lab.hypermarket.app.Good;
import com.epam.lab.hypermarket.app.Material;
import com.epam.lab.hypermarket.app.Type;

public class SheetMaterials extends Good {

	private int depth;
	private int length;
	private int width;
	private int weigth;
	private Material material;

	public SheetMaterials(String name, Country country, double price, Brand brand, int depth, int length, int width,
			int weigth, Material material) {
		super(Type.BUILDING_MATERIAL, name, country, price, brand);
		this.material = material;
		this.width = width;
		this.length = length;
		this.depth = depth;
		this.weigth = weigth;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + depth;
		result = prime * result + length;
		result = prime * result + ((material == null) ? 0 : material.hashCode());
		result = prime * result + weigth;
		result = prime * result + width;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SheetMaterials other = (SheetMaterials) obj;
		if (depth != other.depth)
			return false;
		if (length != other.length)
			return false;
		if (material != other.material)
			return false;
		if (weigth != other.weigth)
			return false;
		if (width != other.width)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + ", depth=" + depth + ", length=" + length + ", width=" + width + ", weigth=" + weigth
				+ ", material=" + material;
	}

}
