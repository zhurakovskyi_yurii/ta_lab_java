package com.epam.lab.hypermarket.householdappliancesandconsumerelectronics;

import com.epam.lab.hypermarket.app.Brand;
import com.epam.lab.hypermarket.app.Country;
import com.epam.lab.hypermarket.app.Good;
import com.epam.lab.hypermarket.app.Type;

/**
 * @author Yurii Zhurakovskyi
 */
public class WashingMachine extends Good {

	private int capacity;
	private int spin_speed;
	private int quick_wash_time;
	private String energy_rating;

	public WashingMachine(String name, Country country, Brand brand, double price, int capacity, int spin_speed,
			int quick_wash_time, String energy_rating) {
		super(Type.CONSUMER_ELECTRONICS_AND_HOUSEHOLD_APPLIANCES, name, country, price, brand);
		this.capacity = capacity;
		this.spin_speed = spin_speed;
		this.quick_wash_time = quick_wash_time;
		this.energy_rating = energy_rating;
	}

	public int getCapacity() {
		return capacity;
	}

	public int getSpin_speed() {
		return spin_speed;
	}

	public int getQuick_wash_time() {
		return quick_wash_time;
	}

	public String getEnergy_rating() {
		return energy_rating;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + capacity;
		result = prime * result + ((energy_rating == null) ? 0 : energy_rating.hashCode());
		result = prime * result + quick_wash_time;
		result = prime * result + spin_speed;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WashingMachine other = (WashingMachine) obj;
		if (capacity != other.capacity)
			return false;
		if (energy_rating == null) {
			if (other.energy_rating != null)
				return false;
		} else if (!energy_rating.equals(other.energy_rating))
			return false;
		if (quick_wash_time != other.quick_wash_time)
			return false;
		if (spin_speed != other.spin_speed)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + ", capacity" + capacity + "kg, Spin speed: " + spin_speed + "rpm, Quick wash time: "
				+ quick_wash_time + " minutes for 2 kg, Energy rating" + energy_rating;
	}
}
