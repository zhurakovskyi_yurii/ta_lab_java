package com.epam.lab.hypermarket.householdappliancesandconsumerelectronics;

import com.epam.lab.hypermarket.app.Brand;
import com.epam.lab.hypermarket.app.Country;
import com.epam.lab.hypermarket.app.Good;
import com.epam.lab.hypermarket.app.Type;

/**
 * @author Yurii Zhurakovskyi
 */
public class Chandelier extends Good {

	private int wide;
	private int high;
	private String model;
	private String type_of_bulb;

	public Chandelier(String name, Country country, Brand brand, double price, int wide, int high, String model,
			String type_of_bulb) {
		super(Type.CONSUMER_ELECTRONICS_AND_HOUSEHOLD_APPLIANCES, name, country, price, brand);
		this.wide = wide;
		this.high = high;
		this.model = model;
		this.type_of_bulb = type_of_bulb;
	}

	public int getWide() {
		return wide;
	}

	public int getHigh() {
		return high;
	}

	public String getModel() {
		return model;
	}

	public String getType_of_bulb() {
		return type_of_bulb;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + high;
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + ((type_of_bulb == null) ? 0 : type_of_bulb.hashCode());
		result = prime * result + wide;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Chandelier other = (Chandelier) obj;
		if (high != other.high)
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (type_of_bulb == null) {
			if (other.type_of_bulb != null)
				return false;
		} else if (!type_of_bulb.equals(other.type_of_bulb))
			return false;
		if (wide != other.wide)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + ", Model: " + model + ", Size: " + wide + "cm Wide x " + high
				+ "cm High, type of Bulb: " + type_of_bulb;
	}
}
