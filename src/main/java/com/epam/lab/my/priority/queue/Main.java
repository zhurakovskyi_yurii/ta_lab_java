package com.epam.lab.my.priority.queue;

import com.epam.lab.ship.with.droids.array.Droid;
import com.epam.lab.ship.with.droids.array.IG_88;
import com.epam.lab.ship.with.droids.array.R2_D2;
import com.epam.lab.ship.with.droids.array.TC;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Iterator;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);
    public static void main(String[] args) {
        DroidsPriorityQueue<Droid> droids = new MyDroidsPriorityQueue<>();
        droids.add(new IG_88());
        droids.add(new R2_D2());
        droids.add(new TC());
        for (int i = 0; i < 5; i++) {
            droids.add(new Droid("droid" + i));
        }
        printAllDroids(droids);
        logger.info("\n\n Try to remove R2_D2\n\n");
        droids.remove(new R2_D2());
        printAllDroids(droids);
        Droid droid = droids.poll();
        logger.info("\n\nTry to delete: " + droid + " \n");
        printAllDroids(droids);
    }

    public static void printAllDroids(DroidsPriorityQueue<Droid> droids){
        Iterator iterator = droids.iterator();
        logger.info("\nAll droids: \n\n");
        while (iterator.hasNext()){
            logger.info(iterator.next() + "\n");
        }
    }
}
