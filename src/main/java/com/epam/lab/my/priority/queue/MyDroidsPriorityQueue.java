package com.epam.lab.my.priority.queue;

import com.epam.lab.ship.with.droids.array.Droid;

import java.util.*;

public class MyDroidsPriorityQueue<T extends Droid> implements DroidsPriorityQueue{
    private int size;
    private transient Object[] droids;

    public void add(Droid droid) {
        if(droids == null)
            droids = new Droid[1];
        else {
            droids = Arrays.copyOf(droids, droids.length + 1);
        }

        droids[droids.length - 1] = droid;
        size = droids.length;
    }

    public void remove(Droid droid) {
        int count = 0;
        for (int i = 0; i < droids.length; i++) {
            if(droids[i].equals(droid)){
                droids[i] = null;
                count++;
            }
        }
        Object[] objects = new Object[size - count];
        int index = 0;
        for (int i = 0; i < droids.length; i++) {
            if(droids[i] != null){
                objects[index] = droids[i];
                index++;
            }
        }
        droids = Arrays.copyOf(objects, size - count);
        size -= count;
    }

    public int size() {
        return size;
    }

    public Object[] toArray(){
        return Arrays.copyOf(droids, size);
    }


    public T poll() {
        if(droids != null) {
            T droid = (T) droids[0];
            for (int i = 0; i < droids.length - 1; i++) {
                droids[i] = droids[i + 1];
            }
            droids = Arrays.copyOf(droids, droids.length - 1);
            size = droids.length;
            return droid;
        }
        return null;
    }


    public T peek() {
        if(droids != null)
            return(T) droids[0];
        return null;
    }

    public Iterator<T> iterator(){
        return new MyDroidsPriorityQueueIterator<>();
    }

    public void clear() {
        size = 0;
        droids = null;
    }

    public boolean isEmpty(){
        return size == 0;
    }

    private final class MyDroidsPriorityQueueIterator<T> implements Iterator<T> {
        private int pointer = 0;
        public boolean hasNext() {
            return pointer < size;
        }

        public T next() {
            return (T) droids[pointer++];
        }
    }
}
