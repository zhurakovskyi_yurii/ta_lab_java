package com.epam.lab.my.priority.queue;

import com.epam.lab.ship.with.droids.array.Droid;

import java.util.Iterator;

public interface DroidsPriorityQueue<T extends Droid> {
    void add(T droid);
    void remove(T droid);
    Object[] toArray();
    T poll();
    T peek();
    Iterator<T> iterator();
    void clear();
    boolean isEmpty();
}
