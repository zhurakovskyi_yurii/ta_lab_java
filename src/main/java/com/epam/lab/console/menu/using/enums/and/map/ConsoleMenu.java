package com.epam.lab.console.menu.using.enums.and.map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

public final class ConsoleMenu {
    private static final Logger logger = LogManager.getLogger(ConsoleMenu.class);
    private Map<DishType, Dish> map = new TreeMap<>();

    public void run() {
        try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            logger.info("\n Enum console view press 1 \n");
            logger.info("\n Map console view 2 \n");
            logger.info("\n Press something else to close\n");
            switch (bufferedReader.readLine().trim()){
                case "1":
                    EnumConsoleView();
                    break;
                case "2":
                    MapConsoleView();
                    break;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static enum DishType {
        BREAKFAST, DINNER, LUNCH, PIZZA, SALADS, SNACKS, SOUPS, DESSERTS
    }

    public static enum Dish {
        APPLE_PIE(DishType.DESSERTS),
        CHOCOLATE_CAKE(DishType.DESSERTS),
        FRUIT_SALAD(DishType.DESSERTS),
        CHICAGO(DishType.PIZZA),
        NEW_YORK(DishType.PIZZA),
        SICILIAN(DishType.PIZZA),
        GREEK(DishType.PIZZA),
        CALIFORNIA(DishType.PIZZA),
        PUREE(DishType.SOUPS),
        VELOUTÉ(DishType.SOUPS),
        BISQUE(DishType.SOUPS),
        CHOWDER(DishType.SOUPS),
        GREEN(DishType.SALADS),
        BOUND(DishType.SALADS),
        BAKED_APPLE_CHIPS(DishType.SNACKS),
        BAKED_SWEET_POTATO_CHIPS(DishType.SNACKS),
        BAKED_SWEET_POTATO(DishType.SNACKS),
        BANANA_ICE_CREAM(DishType.SNACKS),
        ENGLISH(DishType.BREAKFAST),
        CONTINENTAL(DishType.BREAKFAST),
        AMERICAN(DishType.BREAKFAST),
        WHITE_CHICKEN_CHILI(DishType.DINNER),
        CHICKEN_APPLE_PASTA(DishType.DINNER),
        CHILI_MAC(DishType.DINNER),
        GRILLED_LAMB_AND_FETA_PITA_SANDWICHES(DishType.LUNCH),
        THE_ULTIMATE_VEGGIE_SANDWICH(DishType.LUNCH);
        private final DishType dishType;

        Dish(DishType dishType) {
            this.dishType = dishType;
        }

        public DishType getDishType(){
            return dishType;
        }
    }

    public void MapConsoleView() {
        Arrays.stream(Dish.values()).forEach(d -> map.put(d.getDishType(), d));
        showTreeMapValues();
        List<Dish> dishes = saveOrderInList();
        printOrderList(dishes);
    }

    public void showTreeMapValues(){
        logger.info("\n");
        map.entrySet().stream().forEach(e -> logger.info(enumValueToStringLine(e.getKey().name()) + ": " + e.getValue() + " \n"));
    }

    public void EnumConsoleView() {
        Arrays.stream(DishType.values()).forEach(t -> printOneTypeOfDish(t));
        List<Dish> dishes = saveOrderInList();
        printOrderList(dishes);
    }

    private List<Dish> saveOrderInList(){
        List<Dish> dishes = new ArrayList<>();
        dishes.addAll(inputChoice());
        return dishes;
    }

    private void printOrderList(List<Dish> dishes){
        if (!dishes.isEmpty()) {
            logger.info("\nYour order: \n");
            dishes.forEach(d -> logger.info("\n" + d + "\n"));
            logger.info("\nBon appetit!\n");
        }
    }

    private void printOneTypeOfDish(DishType dishType) {
        logger.info("\n" + dishType.name() + ": ");
        Arrays.stream(Dish.values())
                .filter(d -> d.dishType == dishType)
                .forEach(d -> logger.info("\n" + Arrays.stream(d.name().toLowerCase().split("_")).collect(Collectors.joining(" "))));
        logger.info("\n");
    }

    private Dish checkChoiceAndGetDish(String choice) {
        return Arrays.stream(Dish.values()).filter(d -> enumValueToStringLine(d.name()).equalsIgnoreCase(choice)).findFirst().get();
    }

    private String enumValueToStringLine(String enumValue) {
        return Arrays.stream(enumValue.toLowerCase().split("_")).collect(Collectors.joining(" "));
    }

    private List<Dish> inputChoice() {
        logger.info("\nHere you pay 100$ and can eat what you want\n");
        logger.info("\nEnter some name of a dish:\n");
        List<Dish> dishes = new ArrayList<>();
        boolean exit = false;
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            for (int i = 0; i < 10; i++) {
                String choice = bufferedReader.readLine();
                choice = choice.trim();
                if (choice.length() == 0)
                    break;
                dishes.add(checkChoiceAndGetDish(choice));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dishes;
    }
}
