package com.epam.lab.logical.task.d.game;

public interface IbehindTheDoor {
	void show(Hero hero);
	int getPower();
}
