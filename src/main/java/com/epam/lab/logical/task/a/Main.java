package com.epam.lab.logical.task.a;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	private static final Logger logger = LogManager.getLogger(com.epam.lab.comparing.Main.class);
	public static void main(String[] args) throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		int[] array1 = new int[10];
		int[] array2 = new int[10];
		logger.info("Enter the first array..");
		for (int i = 0; i < array1.length; i++) {
			System.out.print("array1[" + i + "] = ");
			try {
				array1[i] = Integer.parseInt(bufferedReader.readLine());
			} catch (NumberFormatException exception) {
				logger.info("You entered an incorrect data");
				array1[i] = -500;
			}
		}
		logger.info("\n");
		for (int i = 0; i < array2.length; i++) {
			logger.info("array2[" + i + "] = ");
			try {
				array2[i] = Integer.parseInt(bufferedReader.readLine());
			} catch (NumberFormatException exception) {
				logger.info("You entered an incorrect data");
			}
		}

		bufferedReader.close();
		MyContainer myContainer = new MyContainer(array1, array2);
		myContainer.createContainersByFirstArray();
		int[] presentInBothArrays = myContainer.getSummaryArrayDuplicates();
		logger.info("\npresent in both arrays:");
		for (int i = 0; i < presentInBothArrays.length; i++) {
			logger.info("presentInBothArrays[" + i + "] = " + presentInBothArrays[i]);
		}
		logger.info("\npresent in only the first of the arrays:");
		int[] uniqueFirst = myContainer.getUniqueInArray();
		for (int i = 0; i < uniqueFirst.length; i++) {
			logger.info("uniqueFirst[" + i + "] = " + uniqueFirst[i]);
		}
	}
	
	
}
