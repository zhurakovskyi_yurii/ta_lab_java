package com.epam.lab.logical.task.d.game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Artifact implements IbehindTheDoor {
	private int power;
	private static final Logger logger = LogManager.getLogger(Artifact.class);

	public Artifact() {
		this.power = 10 + (int) (Math.random() * 80);
	}

	@Override
	public void show(Hero hero) {
		logger.info("Here is an artifact. You got +" + this.power + "power! ");
		hero.setPower(hero.getPower() + this.power);
	}

	public int getPower() {
		return power;
	}
	
	public void setPower(int power) {
		this.power = power;
	}

	@Override
	public String toString() {
		return "Artifact";
	}
}
