package com.epam.lab.logical.task.d.game;

public class Door {
	private IbehindTheDoor ibehindTheDoor;
	private boolean isOpened;

	public Door(IbehindTheDoor ibehindTheDoor) {
		this.ibehindTheDoor = ibehindTheDoor;
	}

	public void open() {
		isOpened = true;
		ibehindTheDoor.show(new Hero("name"));
	}

	public boolean isOpened() {
		return isOpened;
	}

	public IbehindTheDoor getIbehindTheDoor() {
		return ibehindTheDoor;
	}

}
