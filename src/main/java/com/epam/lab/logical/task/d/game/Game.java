package com.epam.lab.logical.task.d.game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class Game {
	private RoundHall roundHall;
	private Hero hero;
	private Random random;
	private static final Logger logger = LogManager.getLogger(Game.class);
	public Game() {
		random = new Random();
		Door[] doors = getRandomDoors();
		roundHall = new RoundHall(doors);
		hero = new Hero("Bob");
	}

	private Door[] getRandomDoors() {
		Door[] doors = new Door[10];
		for (int i = 0; i < doors.length; i++) {
			if (random.nextBoolean())
				doors[i] = new Door(new Monster());
			else
				doors[i] = new Door(new Artifact());
		}
		return doors;
	}

	public RoundHall getRoundHall() {
		return roundHall;
	}

	public void play() {
		showHeroDetail();
		showRoundHall();
		logger.info("\n\nCount of deadly doors: " + calculateDeadlyDoor(roundHall.getDoors().length - 1) + "\n");
	}

	private void showHeroDetail() {
		logger.info("Hero's details:");
		logger.info("Name of hero: " + hero.getName());
		logger.info("Power of hero: " + hero.getPower() + "\n");
	}

	private void showRoundHall() {
		logger.info("the hero came into a round hall....\n");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < 10; i++) {
			logger.info("***********  ");
		}
		logger.info("\n");
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 10; j++) {
				logger.info("*         *  ");
			}
			logger.info("\n");
		}
		for (int i = 0; i < 10; i++) {
			if (i < 9) {
				logger.info("*  DOOR " + (i + 1) + " *  ");
			} else
				logger.info("* DOOR " + (i + 1) + " *  ");
		}
		logger.info("\n");
		for (int j = 0; j < roundHall.getDoors().length; j++) {
			if (roundHall.getDoors()[j].getIbehindTheDoor().toString().length() == 7) {
				logger.info("* " + roundHall.getDoors()[j].getIbehindTheDoor() + " *  ");

			} else
				logger.info("* " + roundHall.getDoors()[j].getIbehindTheDoor() + "*  ");
		}
		logger.info("\n");
		for (int i = 0; i < 10; i++) {
			int power = roundHall.getDoors()[i].getIbehindTheDoor().getPower();
			if (power > 99)
				logger.info("*Power " + power + "*  ");
			else if (power <= 99 && power >= 10)
				logger.info("* Power " + power + "*  ");
			else
				logger.info("* Power " + power + " *  ");
		}
		logger.info("\n");
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 10; j++) {
				logger.info("*         *  ");
			}
			logger.info("\n");
		}
		for (int i = 0; i < 10; i++) {
			logger.info("***********  ");
		}
	}

	private int calculateDeadlyDoor(int doorIndex) {
		if (doorIndex != -1) {
			IbehindTheDoor ibehindTheDoor = roundHall.getDoors()[doorIndex].getIbehindTheDoor();
			if (ibehindTheDoor.toString().equals("Monster") && ibehindTheDoor.getPower() > hero.getPower()) {
				return 1 + calculateDeadlyDoor(doorIndex - 1);
			} else
				return 0 + calculateDeadlyDoor(doorIndex - 1);
		}
		return 0;
	}
}