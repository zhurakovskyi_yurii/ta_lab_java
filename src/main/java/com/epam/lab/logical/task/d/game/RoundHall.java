package com.epam.lab.logical.task.d.game;

public class RoundHall {
	private Door[] doors;
	public RoundHall(Door[] doors) {
		this.doors = doors;
	}
	public Door[] getDoors() {
		return doors;
	}
	
}
