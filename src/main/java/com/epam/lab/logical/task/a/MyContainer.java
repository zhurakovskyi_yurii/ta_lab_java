package com.epam.lab.logical.task.a;

public class MyContainer {
	private int[] array1;
	private int[] array2;
	private int[] summaryArrayDuplicates;
	private int[] uniqueInOneArray;

	public MyContainer(int[] array1, int[] array2) {
		if (array1 != null && array2 != null) {
			this.array1 = array1;
			this.array2 = array2;
		}
	}

	public int[] getSummaryArrayDuplicates() {
		return summaryArrayDuplicates;
	}

	public int[] getUniqueInArray() {
		return uniqueInOneArray;
	}
	

	public void createContainersByFirstArray() {
		if (array1 != null && array2 != null) {
			int size = 0;
			if (array1.length > array2.length)
				size = array1.length;
			else
				size = array2.length;
			summaryArrayDuplicates = new int[size];
			uniqueInOneArray = new int[size];
			int index1 = 0, index2 = 0;
			for (int i = 0; i < array1.length; i++) {
				boolean b = false;
				for (int j = 0; j < array2.length; j++) {
					if (array1[i] == array2[j]) {
						b = true;
					}
				}
				if (b) {
					summaryArrayDuplicates[index1] = array1[i];
					index1++;
				} else {
					uniqueInOneArray[index2] = array1[i];
					index2++;
				}
			}
		}
	}
	
	public void createContainersBySecondArray() {
		if (array2 != null && array1 != null) {
			int size = 0;
			if (array2.length > array1.length)
				size = array2.length;
			else
				size = array1.length;
			summaryArrayDuplicates = new int[size];
			uniqueInOneArray = new int[size];
			int index1 = 0, index2 = 0;
			for (int i = 0; i < array2.length; i++) {
				boolean b = false;
				for (int j = 0; j < array1.length; j++) {
					if (array2[i] == array1[j]) {
						b = true;
					}
				}
				if (b) {
					summaryArrayDuplicates[index1] = array2[i];
					index1++;
				} else {
					uniqueInOneArray[index2] = array2[i];
					index2++;
				}
			}
		}
	}

	public int[] removeNulls(int[] array) {
		int size = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] != 0) {
				size++;
			}
		}

		int[] array2 = new int[size];
		int index = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] != 0) {
				array2[index] = array[i];
				index++;
			}
		}
		return array;
	}
}
