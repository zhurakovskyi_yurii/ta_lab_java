package com.epam.lab.lambdas.streams.second.task;

public interface Command {
    void execute();
}
