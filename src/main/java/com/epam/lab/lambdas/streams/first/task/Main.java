package com.epam.lab.lambdas.streams.first.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);
    public static void main(String[] args) {
        IThreeParametersFunction threeParametersFunction1 = (a, b, c) -> (a > b && a > c) ? a : (b > c)? b : c;
        logger.info("Max value of numbers 10, 48, -14: \n");
        logger.info(threeParametersFunction1.accept(10, 48, -14) + "\n");
        IThreeParametersFunction threeParametersFunction2 = (a, b, c) -> (a + b + c) / 3 ;
        logger.info("Average value of numbers 10, 48, -14: \n");
        logger.info(threeParametersFunction2.accept(10, 48, -14) + "\n");
    }
}
