package com.epam.lab.lambdas.streams.second.task;

public enum RoomOrPlace {
    LIVING_ROOM,
    FAMILY_ROOM,
    GUEST_ROOM,
    KITCHEN,
    DINING_ROOM,
    GAME_ROOM,
    BATHROOM,
    GARAGE,
    ATTIC
}
