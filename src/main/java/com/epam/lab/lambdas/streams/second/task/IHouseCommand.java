package com.epam.lab.lambdas.streams.second.task;

public interface IHouseCommand {
    void turningOnAnAlarmSystem(RoomOrPlace roomOrPlace);
    void cookingSomething(String dish);
    void cleaningRoom(RoomOrPlace roomOrPlace);
    void parkingCar();
}
