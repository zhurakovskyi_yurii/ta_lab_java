package com.epam.lab.lambdas.streams.second.task;

import java.util.ArrayList;
import java.util.List;

public class Robot {
    private List<Command> commandList = new ArrayList<>();
    public void addCommand(Command command){
        commandList.add(command);
    }
    public void run(){
        for (Command command : commandList) {
            command.execute();
        }
    }
}
