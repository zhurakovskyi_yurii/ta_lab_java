package com.epam.lab.lambdas.streams.second.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CleaningRoom implements Command {
    private static final Logger logger = LogManager.getLogger(CleaningRoom.class);
    private House house;
    private RoomOrPlace room;

    public CleaningRoom(House house, RoomOrPlace room) {
        this.house = house;
        this.room = room;
    }

    @Override
    public void execute() {
        this.house.cleaningRoom(room);
    }
}
