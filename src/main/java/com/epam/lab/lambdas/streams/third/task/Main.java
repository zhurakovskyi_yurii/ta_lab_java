package com.epam.lab.lambdas.streams.third.task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);
    public static void main(String[] args) {
        List<Integer> integerList1 = getListIntegers1();
        List<Integer> integerList2 = getListIntegers2();
        List<Integer> integerList3 = getListIntegers3();
        List<Integer> integerList4 = getListIntegers4();
        List<Integer> integerList5 = getListIntegers5();
        logger.info("\nList1: \n");
        printList(integerList1);
        logger.info("\nList2: \n");
        printList(integerList2);
        logger.info("\nList3: \n");
        printList(integerList3);
        logger.info("\nList4: \n");
        printList(integerList4);
        logger.info("\nList5: \n");
        printList(integerList5);
        logger.info("\n");
        logger.info("\nList1: \n");
        printTaskList(integerList1);
        logger.info("\nList2: \n");
        printTaskList(integerList2);
        logger.info("\nList3: \n");
        printTaskList(integerList3);
        logger.info("\nList4: \n");
        printTaskList(integerList4);
        logger.info("\nList5: \n");
        printTaskList(integerList5);
        logger.info("\n");
    }
    private static void printTaskList(List<Integer> integers){
        logger.info("\n");
        logger.info("count of elements \n");
        logger.info(countOfInts(integers) + "\n");
        logger.info("average of Ints\n");
        logger.info(averageOfInts(integers) + "\n");
        logger.info("min of Ints\n");
        logger.info(minOfInts(integers) + "\n");
        logger.info("max of Ints\n");
        logger.info(maxOfInts(integers) + "\n");
        logger.info("sum of Ints\n");
        logger.info(sumOfInts(integers) + "\n");
        logger.info("sum (using reduce) of Ints\n");
        logger.info(sumOfIntsUsingReduce(integers) + "\n");
        logger.info("count number of values that are bigger than average\n");
        logger.info(countNumberOfValuesThatAreBiggerThanAverage(integers) + "\n");
        logger.info("\n");
    }

    public static void printList(List<Integer> list) {
        logger.info("\n");
        for (int n : list) {
            logger.info(n + "\n");
        }
    }

    public static List<Integer> getListIntegers1() {
        return new Random()
                .ints(-1000, 1000)
                .boxed()
                .limit(10).collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
    }

    public static List<Integer> getListIntegers2() {
        return IntStream.generate(() -> (int) (Math.random() * 1000) - 200)
                .boxed()
                .limit(10).collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
    }

    public static List<Integer> getListIntegers3() {
        return Stream.generate(new Random()::nextInt)
                .limit(10).map(e -> e/100000).collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
    }

    public static List<Integer> getListIntegers4() {
        return ThreadLocalRandom.current().ints()
                .limit(10).map(e -> e/100000).collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
    }

    public static List<Integer> getListIntegers5() {
        return Stream.iterate(-1000, n -> n + 230)
                .limit(10)
                .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
    }

    public static int countOfInts(List<Integer> integers){
        return (int) integers.stream().count();
    }

    public static int averageOfInts(List<Integer> integers){
        return (int) integers.stream().mapToInt(n -> (int) n).average().getAsDouble();
    }

    public static int minOfInts(List<Integer> integers){
        return integers.stream().min(Integer::compareTo).get();
    }

    public static int maxOfInts(List<Integer> integers){
        return integers.stream().max(Integer::compareTo).get();
    }

    public static int sumOfInts(List<Integer> integers){
        return integers.stream().mapToInt(e -> e).sum();
    }

    public static int sumOfIntsUsingReduce(List<Integer> integers){
        return integers.stream().reduce((n1, n2) -> n1 + n2).get();
    }

    public static int countNumberOfValuesThatAreBiggerThanAverage(List<Integer> integers){
        return (int) integers.stream().filter(n -> n > averageOfInts(integers)).count();
    }
}
