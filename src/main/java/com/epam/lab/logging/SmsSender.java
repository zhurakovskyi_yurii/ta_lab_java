package com.epam.lab.logging;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SmsSender {
    public static final String ACCOUNT_SID = "AC0d82eb327db27aa85980599293fc97cb";
    public static final String AUTH_TOKEN = "55f5b99417b1911a66e27a4a442b2e55";

    public static void send(String string) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380955030740"),
                        new PhoneNumber("+14158141829"),
                        string)
                .create();

        System.out.println(message.getSid());
    }
}