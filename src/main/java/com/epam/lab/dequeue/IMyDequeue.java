package com.epam.lab.dequeue;

import com.epam.lab.ship.with.droids.array.Droid;

import java.util.Iterator;

public interface IMyDequeue<T> {
    void add(T elem);
    void addFirst(T elem);
    void addLast(T elem);
  /*  T remove(int index);
      T poll();
    T pollFirst();
    T pollLast();
    T peekFirst();
    T peekLast();
    T peek();
    Iterator<T> iterator();
    void clear();
    boolean isEmpty(); */
}