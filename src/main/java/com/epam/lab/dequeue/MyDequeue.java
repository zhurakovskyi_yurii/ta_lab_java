package com.epam.lab.dequeue;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyDequeue<T> implements IMyDequeue<T> {
    private transient Node<T> firstNode;
    private transient Node<T> lastNode;
    private int size = 0;

    public MyDequeue() {
    }

    @Override
    public void add(T elem) {
        addLast(elem);
    }

    @Override
    public void addFirst(T elem) {
        Node<T> newNode = new Node<T>(elem);
        if (firstNode != null) {
            firstNode.prevNode = newNode;
            newNode.nextNode = firstNode;
            firstNode = newNode;
        } else {
            firstNode = lastNode = newNode;
        }
        ++size;
    }

    @Override
    public void addLast(T elem) {
        Node<T> newNode = new Node<T>(elem);
        if (lastNode != null) {
            lastNode.nextNode = newNode;
            newNode.prevNode = lastNode;
            lastNode = newNode;
        } else {
            firstNode = lastNode = newNode;
        }
        ++size;
    }

 /*   @Override
    public T remove(int index) {
        if(index < 0 && index >= size)
            new NoSuchElementException("This element doesn't exist");
        Node<T> node = firstNode;
        int counter = 0;
        while (node.nextNode != null){
            if(counter != index) {
                node = node.nextNode;
                counter++;
            } else {
                Node<T> prev = node.prevNode;
                Node<T> next = node.nextNode;
                prev.nextNode = next;
                next.prevNode = prev;
                node = null;
                break;
            }
        }
        return (T)node;
    }
    */

    public T get(int index){
        if(index < 0 && index >= size)
            new IndexOutOfBoundsException("This index doesn't exist");
        Node<T> node = firstNode;
        int counter = 0;
        while (node.nextNode != null){
            if(counter != index) {
                node = node.nextNode;
                counter++;
            } else {
                break;
            }
        }
        return (T)node;
    }


    public boolean isEmpty() {
        return size == 0;
    }

    public Iterator iterator() {
        return new MyDequeueIterator();
    }

    public int size() {
        return size;
    }

    private final static class Node<T> {
        T current;
        Node<T> prevNode;
        Node<T> nextNode;

        public Node(T current) {
            this.current = current;
        }

        @Override
        public String toString() {
            return current.toString();
        }
    }
    private final class MyDequeueIterator<T> implements Iterator<T>{
        Node<T> thisElem = (Node<T>) firstNode;
        private int pointer = 0;
        @Override
        public boolean hasNext() {
            return pointer < size;
        }

        @Override
        public T next() {
            if (!hasNext())
                new NoSuchElementException();
            ++pointer;
            Node<T> elem = thisElem;
            thisElem = thisElem.nextNode;
            return (T) elem;
        }
    }
}
