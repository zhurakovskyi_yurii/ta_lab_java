package com.epam.lab.add.string.to.integers.list;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);
    public static void main(String[] args) {
        List list = new ArrayList();
        list.add("some string");
        list.add(2);
        List<Integer> integerList = new ArrayList<>();
        integerList = list;
        logger.info(integerList.get(0));
       // logger.info( integerList.get(0)*80); //java.lang.ClassCastException:
        logger.info(integerList.get(1)/1.5);
    }
}
