package com.epam.lab.exception.practical.tasks.second;

public class ColorException extends Exception {
	public ColorException(String msg) {
		super(msg);
	}

	public ColorException() {
	}
}
