package com.epam.lab.exception.practical.tasks.second;

public class TypeException extends Exception {
	public TypeException(String msg) {
		super(msg);
	}

	public TypeException() {
	}
}
