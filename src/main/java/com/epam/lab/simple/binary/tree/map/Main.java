package com.epam.lab.simple.binary.tree.map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);
    public static void main(String[] args) {
      SimpleBinaryTreeMap<String, String> simpleBinaryTreeMap = new SimpleBinaryTreeMap<>();
        for (int i = 0; i < 20; i++) {
            simpleBinaryTreeMap.put("key" + i, "value" + i);
        }
        for (int i = 0; i < 20; i++) {
            logger.info(simpleBinaryTreeMap.get("key" + i) + "\n");
        }
    }
}
