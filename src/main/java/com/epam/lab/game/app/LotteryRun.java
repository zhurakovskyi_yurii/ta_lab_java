package com.epam.lab.game.app;

import java.util.Date;

import com.epam.lab.game.entity.*;

public class LotteryRun {
	public static void view() {
		System.out.println("\n------------------------Lottery-----------------------------------------\n");
		System.out.println("Menu:");
		System.out.println("1 Rules");
		System.out.println("2 Buy a ticket");
		System.out.println("3 Play");
		System.out.println("4 Exit.");
		System.out.println("Enter your choice");
	}

	public static void rules() {
		System.out.println(
				"You should choose \"2 Buy a ticket\" and then choose \"3 Play\". We'll check your ticket and say the result of the lottery");
	}

	public static void buyTicket(ILotteryUser user) {
		user.buyTicket(new Ticket(NumberGenerator.getInstance().getNumber() + "", 100, new Date()));
	}

	public static void play(Ticket ticket) {
		ILottery lottery = new Lottery(WinningTickets.getWinnNumbers());
		System.out.println("\nWinnig number of tickets: \n");
		for (String number : lottery.getWinnNumbers()) {
			System.out.println(number);
		}
		if (lottery.checkTicket(ticket)) {
			System.out.println("Your ticket: " + ticket.getNumber() + ", winning numbers: "
					+ ticket.getCountWinnNumbers() + ", You won " + ticket.getPrize() + "hrn!");
		} else
			System.out.println("Your ticket: " + ticket.getNumber() + ", You lose!");
	}
}
