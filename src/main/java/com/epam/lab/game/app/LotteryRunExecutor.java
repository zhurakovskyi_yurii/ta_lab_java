package com.epam.lab.game.app;

import com.epam.lab.game.entity.ILotteryUser;
import com.epam.lab.game.entity.Ticket;
import com.epam.lab.game.entity.User;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LotteryRunExecutor {
    public static void execute() {
        InputStreamReader inputStreamReader = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        User user = new User();
        while (true) {

            LotteryRun.view();
            try {
                String choose = bufferedReader.readLine();
                switch (choose) {
                    case "1":
                        LotteryRun.rules();
                        break;
                    case "2":
                        System.out.println("Enter your firstname: ");
                        String firstName;
                        firstName = bufferedReader.readLine();
                        System.out.println("Enter your lastname: ");
                        String lastName = bufferedReader.readLine();
                        System.out.println("Enter your age: ");
                        int age = Integer.parseInt(bufferedReader.readLine());
                        user = new User(firstName, lastName, age);
                        LotteryRun.buyTicket(user);
                        user.getTickets().forEach(System.out::println);
                        break;
                    case "3":
                        user.getTickets().forEach(System.out::println);
                        for (Ticket ticket : user.getTickets()) {
                            LotteryRun.play(ticket);
                        }
                        break;
                    case "4":
                        inputStreamReader.close();
                        bufferedReader.close();
                        System.exit(0);
                        break;
                    default:
                        System.out.println("\nIncorrect choice\n");
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
