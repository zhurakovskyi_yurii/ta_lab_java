package com.epam.lab.game.entity;

import java.util.Collection;

public interface ILottery {
	public Collection<Ticket> getWinningTickets(Collection<Ticket> tickets);
	public boolean checkTicket(Ticket ticket);
	public String[] getWinnNumbers();
}
