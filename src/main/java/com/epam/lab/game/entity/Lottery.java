package com.epam.lab.game.entity;

import java.util.Collection;
import java.util.stream.Collectors;

public class Lottery implements ILottery {
	private String[] winnNumbers;

	public Lottery(String[] winnNumbers) {
		this.winnNumbers = winnNumbers;
	}

	public Collection<Ticket> getWinningTickets(Collection<Ticket> tickets) {
		return tickets.stream().filter(t -> checkTicket(t)).collect(Collectors.toList());
	}

	public String[] getWinnNumbers() {
		return winnNumbers;
	}

	public boolean checkTicket(Ticket ticket) {
		boolean result = false;
		int max = 0, tmp = 0;
		for (String n : winnNumbers) {
			tmp = 0;
			for (int i = 0; i < n.toCharArray().length; i++) {
				if (ticket.getNumber().contains("" + n.toCharArray()[i])) {
					tmp++;
					result = true;
				}
			}
			if (max < tmp) {
				max = tmp;
			}
		}
		if (result) {
			ticket.setCountWinnNumbers(max);
			ticket.setPrize(max * 5);
		}
		return result;
	}
}
