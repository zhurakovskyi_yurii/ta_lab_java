package com.epam.lab.game.entity;

public interface ILotteryUser {
    public void buyTicket(Ticket ticket);
}
