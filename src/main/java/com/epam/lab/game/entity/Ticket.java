package com.epam.lab.game.entity;

import java.util.Date;

public class Ticket {
	private String number;
	private int price;
	private Date endDate;
	private int countWinnNumbers;
	private int prize;

	public Ticket(String number, int price, Date endDate) {
		while (number.length() < 6) {
			number = "0" + number;
		}

		this.number = number;
		this.price = price;
		countWinnNumbers = 0;
		this.endDate = endDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public int getPrize() {
		return prize;
	}

	public int getCountWinnNumbers() {
		return countWinnNumbers;
	}

	public void setCountWinnNumbers(int countWinnNumbers) {
		this.countWinnNumbers = countWinnNumbers;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getNumber() {
		return number;
	}

	public int getPrice() {
		return price;
	}

	public void setPrize(int prize) {
		this.prize = prize;
	}

	@Override
	public String toString() {
		return "Ticket [number=" + number + ", price=" + price + ", endDate=" + endDate + ", countWinnNumbers="
				+ countWinnNumbers + ", prize=" + prize + "]";
	}

}
