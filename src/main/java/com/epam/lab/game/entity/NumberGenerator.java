package com.epam.lab.game.entity;

public class NumberGenerator {
	private static NumberGenerator instance;

	private NumberGenerator() {
	}

	public static NumberGenerator getInstance() {
		if (instance == null)
			instance = new NumberGenerator();
		return instance;
	}

	public int getNumber() {
		return (int) (Math.random() * 1000000);
	}

	public int[] getTenNumbers() {
		int[] numbers = new int[10];
		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = (int) (Math.random() * 1000000);
		}
		return numbers;
	}
}
